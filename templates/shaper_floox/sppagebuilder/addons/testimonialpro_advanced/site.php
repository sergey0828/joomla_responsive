<?php

/**
 * @package SP Page Builder
 * @author JoomShaper http://www.joomshaper.com
 * @copyright Copyright (c) 2010 - 2017 JoomShaper
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
 */
//no direct accees
defined('_JEXEC') or die('resticted aceess');

class SppagebuilderAddonTestimonialpro_advanced extends SppagebuilderAddons {

    public function render() {
        $title = (isset($this->addon->settings->title) && $this->addon->settings->title) ? $this->addon->settings->title : '';
        $heading_selector = (isset($this->addon->settings->heading_selector) && $this->addon->settings->heading_selector) ? $this->addon->settings->heading_selector : '';
        //$text = (isset($this->addon->settings->text) && $this->addon->settings->text) ? $this->addon->settings->text : '';
        //$designation = (isset($this->addon->settings->designation) && $this->addon->settings->designation) ? $this->addon->settings->designation : '';
        $alignment = (isset($this->addon->settings->alignment) && $this->addon->settings->alignment) ? $this->addon->settings->alignment : '';
        $autoplay = (isset($this->addon->settings->autoplay) && $this->addon->settings->autoplay) ? $this->addon->settings->autoplay : '';
        $carousel_autoplay = (isset($this->addon->settings->carousel_autoplay) && $this->addon->settings->carousel_autoplay) ? $this->addon->settings->carousel_autoplay : '';
        $arrows = (isset($this->addon->settings->arrows) && $this->addon->settings->arrows) ? $this->addon->settings->arrows : '';
        $controllers = (isset($this->addon->settings->controllers) && $this->addon->settings->controllers) ? $this->addon->settings->controllers : 'true';
        $class = (isset($this->addon->settings->class) && $this->addon->settings->class) ? $this->addon->settings->class : '';


        //output start
        $output = '';
        $carousel_autoplay = ($autoplay) ? 'data-sppb-ride="sppb-carousel"' : '';
        $output .= '<div class="sppb-carousel sppb-testimonial-pro-advanced sppb-slide ' . $class . ' sppb-text-center" ' . $carousel_autoplay . '>';

        $output .= '<div class="sppb-row">';
        $output .= '<div class="sppb-col-sm-12 ' . $alignment . ' ' . $class . '">';

        if ($title) {
            $output .= '<' . $heading_selector . ' class="sppb-addon-title">' . $title . '</' . $heading_selector . '>';
        }

        // $output .= '<div class="sppb-addon-content">';
        // $output .= $text;
        // $output .= '</div>'; //.sppb-addon-content

        if ($controllers) {
            $output .= '<ol class="sppb-carousel-indicators">';
            foreach ($this->addon->settings->sp_testimonialpro_advanced_item as $key => $slideItem) {
                $active_item = ($key == 0) ? 'active' : '';
                $output .= '<li data-sppb-target=".sppb-testimonial-pro-advanced" class="sppb-tm-indicators ' . $active_item . '" data-sppb-slide-to="' . $key . '">';
                $output .= '<img src="' . $slideItem->avatar . '" alt="' . $slideItem->title . '"/>';
                $output .= '</li>';
            }
            $output .= '</ol>';
        }

        $output .= '<div class="sppb-carousel-inner">';

        foreach ($this->addon->settings->sp_testimonialpro_advanced_item as $key => $slideItem) {

            $output .= '<div class="sppb-item">';
            $output .= '<div class="sppb-testimonial-message">' . $slideItem->message . '</div>';

            $title = '<strong class="pro-client-name">' . $slideItem->title . '</strong>';

            if ($slideItem->url)
                $title .= ' - <span class="pro-client-url">' . $slideItem->url . '</span>';
            //if($slideItem->avatar) $output .= '<img class="sppb-img-responsive sppb-avatar '. $slideItem->avatar_style .'" src="'. $slideItem->avatar .'" alt="">';
            if ($slideItem->title)
                $output .= '<div class="sppb-testimonial-client">' . $slideItem->title . '</div>';
            $output .= '<div class="sppb-testimonial-client-designation">' . $slideItem->designation . '</div>';

            $output .= '</div>';
        }

        //$output .= AddonParser::spDoAddon($content);
        $output .= '</div>';

        if ($arrows) {
            $output .= '<a class="left sppb-carousel-control" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>';
            $output .= '<a class="right sppb-carousel-control" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>';
        }

        $output .= '</div>'; //.sppb-carousel-inner
        $output .= '</div>'; //.sppb-col-sm-12
        $output .= '</div>'; //.sppb-row

        return $output;
    }

    public static function getTemplate() {
        $output = '
                <#
                    var title = (data.title !=="undefined") ? data.title : "";
                    var heading_selector = (data.heading_selector !=="undefined") ? data.heading_selector : "";
                    var alignment = (data.alignment !=="undefined") ? data.alignment : "";
                    var arrows = (data.arrows !=="undefined") ? parseInt(data.arrows) : true;
                    var contentClass = (data.class !=="undefined") ? data.class : "";
                    var autoplay = (data.autoplay !=="undefined") ? \'data-sppb-ride="sppb-carousel"\':"";
                #>

                <div class="sppb-carousel sppb-testimonial-pro-advanced sppb-slide {{contentClass}} sppb-text-center" {{{autoplay}}}>

                <div class="sppb-row">
                <div class="sppb-col-sm-12 {{alignment}} {{contentClass}}">

                <# if (title) { #>
                    <{{{heading_selector}}} class="sppb-addon-title">{{title}}</{{{heading_selector}}}>
                <# } #>

                    <ol class="sppb-carousel-indicators">
                    <#
                        _.each(data.sp_testimonialpro_advanced_item, function(item,key){
                        var activeClass;
                        if (key == 0) {
                                activeClass = " active"
                        }else{
                                activeClass = ""
                        }
                    #>
                    <li data-sppb-target=".sppb-testimonial-pro-advanced" class="sppb-tm-indicators {{ activeClass }}" data-sppb-slide-to="{{ key }}">
                        <img src=\'{{item.avatar}}\' alt="{{item.title}}"/>
                    </li>
                    <# }) #>
                    </ol>

                <div class="sppb-carousel-inner">
                <# _.each (data.sp_testimonialpro_advanced_item, function (slideItem, index) {
                    let slideActClass = ""
                    if (index == 0) {
                            slideActClass = " active"
                    } else {
                            slideActClass = ""
                    }
                #>

                    <div class="sppb-item {{slideActClass}}">
                        <div class="sppb-testimonial-message">{{slideItem.message}}</div>
                        <#
                        var title = \'<strong class="pro-client-name"> {{slideItem.title}} </strong>\';
                        #>

                        <# if (slideItem.url){
                            title += \' - <span class="pro-client-url">{{slideItem.url}}</span>\';
                         } #>

                        <# if (slideItem.title){ #>
                        <div class="sppb-testimonial-client">{{slideItem.title}}</div>
                        <# } #>
                        <div class="sppb-testimonial-client-designation">{{slideItem.designation}}</div>
                    </div>
                <# }) #>
                </div>
                <# if (arrows) { #>
                    <a class="left sppb-carousel-control" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                    <a class="right sppb-carousel-control" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
                <# } #>
                </div>

                </div>
                </div>
                ';
        return $output;
    }

}
