<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('JPATH_BASE') or die;


jimport('joomla.user.helper');
$userId = $displayData['item']->created_by;
$userInfo = JFactory::getUser($userId);
$userProfile = JUserHelper::getProfile($displayData['item']->created_by);
$userName = JFactory::getUser($displayData['item']->created_by)->username;
$socialProfileInstalled = JPluginHelper::getPlugin('user', 'spsocialprofile');
?>

<div class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">

    <div class="author-img">
        <?php if ($displayData['item']->contact_link) { ?>
            <a href="<?php echo $displayData['item']->contact_link; ?>" class="entry-author-link">
                <img itemprop="name" data-toggle="tooltip" title="<?php echo JText::sprintf('COM_CONTENT_WRITTEN_BY', '') . ' ' . $displayData['item']->author; ?>" alt="<?php echo $displayData['item']->author; ?>" src="http://www.gravatar.com/avatar/<?php echo md5(strtolower(trim($userInfo->email))); ?>?s=100">
                <?php $author = ($displayData['item']->created_by_alias ? $displayData['item']->created_by_alias : $displayData['item']->author); ?>
            </a>
        <?php } else { ?>
            <img itemprop="name" data-toggle="tooltip" title="<?php echo JText::sprintf('COM_CONTENT_WRITTEN_BY', '') . ' ' . $displayData['item']->author; ?>" alt="<?php echo $displayData['item']->author; ?>" src="http://www.gravatar.com/avatar/<?php echo md5(strtolower(trim($userInfo->email))); ?>?s=100">
            <?php $author = ($displayData['item']->created_by_alias ? $displayData['item']->created_by_alias : $displayData['item']->author); ?>
        <?php } ?>
    </div> <!-- //.author-img -->

    <div class="author-details">
        <?php $author = ($displayData['item']->created_by_alias ? $displayData['item']->created_by_alias : $displayData['item']->author); ?>
        <?php $author = '<span itemprop="name" data-toggle="tooltip" title="' . JText::sprintf('COM_CONTENT_WRITTEN_BY', '') . '">' . $author . '</span>'; ?>
        <?php if (!empty($displayData['item']->contact_link) && $displayData['params']->get('link_author') == true) : ?>
            <?php echo JHtml::_('link', $displayData['item']->contact_link, $author, array('itemprop' => 'url')); ?>
        <?php else : ?>
            <?php echo $author; ?> 
        <?php endif; ?>

        <?php if ($userName) { ?>
            <p><?php echo $userName; ?></p>
        <?php } ?>

        <?php
        if ($socialProfileInstalled) {
            $author_socials = $userProfile->spsocialprofile;
            $total_socials = count(array_filter($author_socials));

            $author_facbook = (isset($author_socials['facebook']) && $author_socials['facebook']) ? $author_socials['facebook'] : '';
            $author_twitter = (isset($author_socials['twitter']) && $author_socials['twitter']) ? $author_socials['twitter'] : '';
            $author_gplus = (isset($author_socials['gplus']) && $author_socials['gplus']) ? $author_socials['gplus'] : '';
            $author_linkedin = (isset($author_socials['linkedin']) && $author_socials['linkedin']) ? $author_socials['linkedin'] : '';

            if ($author_facbook || $author_twitter || $author_gplus || $author_linkedin) {
                ?>
                <ul class="author-social-link total-item-<?php echo $total_socials; ?>">
                    <?php if ($author_facbook) { ?>
                        <li>
                            <a href="<?php echo $author_facbook; ?>">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                    <?php } if ($author_twitter) { ?>
                        <li>
                            <a href="<?php echo $author_twitter; ?>">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                    <?php } if ($author_gplus) { ?>
                        <li>
                            <a href="<?php echo $author_gplus; ?>">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>
                    <?php } if ($author_linkedin) { ?>
                        <li>
                            <a href="<?php echo $author_linkedin; ?>">
                                <i class="fa fa-linkedin-square"></i>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            <?php }
        }
        ?>
    </div> <!-- //.author-info -->

</div>