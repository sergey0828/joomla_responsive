$('.mega-menu-solutions').hover(
    function(){
        $("#mega-menu-solutions").removeClass('mega-menu-hidden');
    }, function(){
        $("#mega-menu-solutions").addClass('mega-menu-hidden');
    }
);
$('.mega-menu-products').hover(
    function(){
        $("#mega-menu-products").removeClass('mega-menu-hidden');
    },
    function(){
        $("#mega-menu-products").addClass('mega-menu-hidden');
    }
);
$('.mega-menu-company').hover(
    function(){
        $("#mega-menu-company").removeClass('mega-menu-hidden');
    },
    function(){
        $("#mega-menu-company").addClass('mega-menu-hidden');
    }
);


$('.mega-menu-solutions .responsive-menu-btn').click(()=>{
    if($('.mega-menu-solutions .menu-content').hasClass('activate-menu')){
        $('.mega-menu-solutions .menu-content').removeClass('activate-menu');
        $('.mega-menu-solutions .responsive-menu-1.sub-menu').removeClass('activate-menu');
    } else {
        $('.mega-menu-solutions .menu-content').addClass('activate-menu');
        $('.mega-menu-solutions  .responsive-menu-1.sub-menu').addClass('activate-menu');
    }
});

$('.mega-menu-products .responsive-menu-btn').click(()=>{
    if($('.mega-menu-products .menu-content').hasClass('activate-menu')){
        $('.mega-menu-products .menu-content').removeClass('activate-menu');
        $('.mega-menu-products .responsive-menu-1.sub-menu').removeClass('activate-menu');
    } else {
        $('.mega-menu-products .menu-content').addClass('activate-menu');
        $('.mega-menu-products .responsive-menu-1.sub-menu').addClass('activate-menu');
    }
});

$('.mega-menu-company .responsive-menu-btn').click(()=>{
    if($('.mega-menu-company .menu-content').hasClass('activate-menu')){
        $('.mega-menu-company .menu-content').removeClass('activate-menu');
        $('.mega-menu-company .responsive-menu-1.sub-menu').removeClass('activate-menu');
    } else {
        $('.mega-menu-company .menu-content').addClass('activate-menu');
        $('.mega-menu-company .responsive-menu-1.sub-menu').addClass('activate-menu');
    }
});
