<?php
    $imgpath = JURI::base(true).'/templates/'.$app->getTemplate().'/assets/img/';
    $solutions_data = render_mega_solutions($mega_solutions);
    // $products_data = render_mega_products($mega_products);
    // var_dump($products_data);
    
//     $app = JFactory::getApplication();
//     $menu = $app->getMenu();
//     $items = $menu->getItems('menutype', 'mainmenu');
// var_dump($items);exit;
    // $items = $menu->getItems(array('menutype','level'), array('mainmenu',array("1","2")));
    
    // foreach($items as $i=>$item){
    //     echo $i;
    //     var_dump($item);
    //     exit;
    // }
    // exit;


$output .= '
    <section id="mega-menu-solutions" class="container-fluid mega-menu mega-menu-solutions mega-menu-hidden">
        <div class="section-inner">
            <div class="col-lg-12 menu-top-language-responsive"><img class="icon-language-responsive" src="'.$imgpath.'icon-languages.svg"><span>EN</span><img class="icon-search" src="'.$imgpath.'icon-search.svg"></div><div class="visible responsive-menu-1 sub-menu"><a href="#">Solutions</a><a class="responsive-menu-btn">&nbsp;&nbsp;</a></div>
            <div class="row menu-content">
                <div class="col-lg-3 mega-menu-left dark-section"><img class="menu-icon" src="'.$imgpath.'icon-menu-solution.svg">
                    <p class="menu-title-1">Solutions</p>
                    <p class="menu-description">Optical Transport, Access and Software-Defined.&nbsp;A comprehensive portfolio to empower your network !</p>
                </div>
                <div class="col-lg-9 mega-menu-right">
                    <div class="row">
                        <div class="col-lg-4 menu-item-column">
                            <p class="menu-title-2">'.$solutions_data[0]['title'].'</p>'.$solutions_data[0]['data'].'
                        </div>
                        <div class="col-lg-4 menu-item-column">
                            <p class="menu-title-2">'.$solutions_data[1]['title'].'</p>'.$solutions_data[1]['data'].'
                        </div>
                        <div class="col-lg-4 menu-item-column">
                            <p class="menu-title-2">'.$solutions_data[2]['title'].'</p>'.$solutions_data[2]['data'].'
                        </div>
                        <div class="col-lg-12 menu-cta-wrapper">
                            <div class="menu-cta-inner"><a class="cta-orange cta-small" href="#">Solutions Overview</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
';






$output .= '
    <section id="mega-menu-products" class="container-fluid mega-menu mega-menu-products mega-menu-hidden">
        <div class="section-inner"><div class="visible responsive-menu-1 sub-menu"><a href="#">Products &amp; Services</a><a class="responsive-menu-btn">&nbsp;&nbsp;</a></div>
            <div class="row menu-content">
                <div class="col-lg-2 mega-menu-left dark-section"><img class="menu-icon" src="'.$imgpath.'icon-menu-products-services.svg">
                    <p class="menu-title-1">Products &amp; Services</p>
                    <p class="menu-description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. </p>
                </div>
                <div class="col-lg-10 mega-menu-right">
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="row">
                                <div class="col-lg-12 menu-item-column">
                                    <p class="menu-title-4"><a href="#">Products<br></a></p>
                                </div>
                                <div class="col-lg-4 menu-item-column">
                                    <p class="menu-title-2"><a href="#">Ekinops360 Products<br></a></p><a class="menu-title-3" href="#">WDM Transport<br></a><a href="#">White Box</a><a href="#">FlexRate</a><a href="#">10G</a><a href="#">OTN Integration</a><a href="#">Optical Line System</a>
                                    <a href="#">Extended Temperature Range<br></a><a class="menu-title-3" href="#">OTN<br></a><a href="#">ETS12 &amp; Line Modules</a></div>
                                <div class="col-md-auto col-lg-4 menu-item-column">
                                    <p class="menu-title-2"><a href="#">OneAccess Products<br></a></p><a class="menu-title-3" href="#">MSAR<br></a><a href="#">DATA<br></a><a href="#">VOICE</a><a class="menu-title-3" href="#">UCPE<br></a><a href="#">OVP </a><a href="#">VEP<br></a>
                                    <a class="menu-title-3" href="#">EAD</a><a href="#">EAD COPPER</a><a href="#">EAD FIBER TO 1G</a><a href="#">EAD FIBER 10G</a></div>
                                <div class="col-lg-4 menu-item-column">
                                    <p class="menu-title-2"><a href="#">Compose Products<br></a></p><a class="menu-title-3" href="#">Ekinops360</a><a href="#">Management Celestis NMS</a><a class="menu-title-3" href="#">OneAccess</a><a href="#">Compose Manager</a><a href="#">Compose Services</a>
                                    <a href="#">Compose Middleware</a>
                                </div>
                                <div class="col-lg-12 menu-cta-wrapper">
                                    <div class="menu-cta-inner"><a class="cta-orange cta-small" href="#">Products Overview</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 mega-menu-inside-right">
                            <div class="row">
                                <div class="col-lg-12 menu-item-column">
                                    <p class="menu-title-4"><a href="#">Services<br></a></p>
                                </div>
                                <div class="col-lg-12 menu-item-column">
                                    <p class="menu-title-2"><a href="#">Ekinops360 Services<br></a></p><a href="#">Plan<br></a><a href="#">Build<br></a><a href="#">Operate</a><a href="#">Support</a></div>
                                <div class="col-lg-12 menu-item-column">
                                    <p class="menu-title-2"><a href="#">OneAccess Services<br></a></p><a href="#">Support Services</a><a href="#">Security Services</a></div>
                                <div class="col-lg-12 menu-item-column">
                                    <p class="menu-title-2"><a href="#">Compose Services<br></a></p><a href="#">Virtualization Services</a></div>
                                <div class="col-lg-12 menu-cta-wrapper">
                                    <div class="menu-cta-inner"><a class="cta-orange cta-small" href="#">Services Overview</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="responsive-menu-1" href="#">Technologies</a>
        </div>
    </section>
';

$output .= '
    <section id="mega-menu-company" class="container-fluid mega-menu mega-menu-company mega-menu-hidden">
        <div class="section-inner"><div class="visible responsive-menu-1 sub-menu"><a href="#">Company</a><a class="responsive-menu-btn">&nbsp;&nbsp;</a></div>
            <div class="row menu-content">
                <div class="col-lg-3 mega-menu-left dark-section"><img class="menu-icon" src="'.$imgpath.'icon-menu-company.svg">
                    <p class="menu-title-1">Company</p>
                    <p class="menu-description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. </p>
                </div>
                <div class="col-lg-9 mega-menu-right">
                    <div class="row">
                        <div class="col-lg-4 menu-item-column">
                            <p class="menu-title-2"><a href="#">Company</a></p><a href="#">About Ekinops<br></a><a href="#">Careers​</a><a href="#">Executive team</a><a href="#">Contact us</a><a href="#">Offices</a><a href="#">Support​</a></div>
                        <div class="col-lg-4 menu-item-column">
                            <p class="menu-title-2"><a href="#">News &amp; Events</a></p><a href="#">News<br></a><a href="#">Webinars</a><a href="#">Events</a><a href="#">Blog</a></div>
                        <div class="col-lg-8 menu-cta-wrapper">
                            <div class="menu-cta-inner"><a class="cta-orange cta-small" href="#">Company Overview</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 menu-top-items-responsive"><a href="#">Login</a><a href="#">Support</a><a href="#">Parnters</a><a href="#">Investors</a><a href="#">Blog</a><a href="#">Contact</a></div>
        </div>
    </section>
';


    function render_mega_solutions($data) {
        $flag = true;
        $solutions_data = [];
        $i = 0;
        while($flag){
            $item = '<div';
            $start = '<ul';
            $end = '</ul>';
            $result = fetch_menu_contents($data, $item, $start, $end);
            $flag = $result != "";
            $link_pos = strpos($result, '<a');
            if($link_pos!=""){
                $solutions_data[$i] = [];
                $solutions_data[$i]['data'] = '';
                $j = 0;
                while($link_pos>0){
                    $url = substr($result, $link_pos, strpos($result, '</a>')+4-$link_pos);
                    if($j==0){
                        $solutions_data[$i]['title'] = $url;
                    } else {
                        $solutions_data[$i]['data'] .= $url;
                    }
                    $result = substr($result, strpos($result, '</a>')+4);
                    $link_pos = strpos($result, '<a');
                    $j++;
                }
                $i++;
            }

        }
        return $solutions_data;
    }

    function render_mega_products(&$data) {
        $flag = true;
        $products_data = [];
        $i = 0;
        while($flag){
            $item = '';
            $start = '<ul';
            $end = '</ul>';
            $flag = strpos($data, '<ul') != "";
            $result = fetch_menu_contents($data, $item, $start, $end);
            if($flag){
                $products_data[$i] = render_mega_items($result);
                $i++;
            }
        }
        return $products_data;
    }

    function render_mega_items(&$data) {
        $flag = true;
        $i = 0;
        $products_data = [];
        while($flag){
            $item = '';
            $start = '<li';
            $end = '</li>';
            $result = fetch_menu_contents($data, $item, $start, $end);
            $flag = strpos($data, '<li');
            $products_data[$i]['title'] = fetch_link($result);
            $products_data[$i]['hasChild'] = (strpos($result, '<ul') !== '');
            if($products_data[$i]['hasChild']) {
                $child_data = fetch_menu_contents($result, $item, '<ul', '</ul>');
                // $products_data[$i]['data'] = $child_data;
                // render_mega_products($result); //render_mega_items($child_data);
            } else { var_dump("===========".$result);
                $products_data[$i]['data'] = [];
                $flag_item = true;
                $j = 0;
                while($flag_item){
                    $link = fetch_link($result);
                    if($link != "") {
                        $products_data[$i]['data'][$j] = $link;
                        $j++;
                    } else {
                        $flag_item = false;
                    }
                }
            }
            $i++;
        }
        return $products_data;
    }

    function fetch_link(&$data) 
    {
        $pos = strpos($data, '<a');
        $result = ($pos === "") ? "" : substr($data, $pos, strpos($data, '</a>')+4-$pos);
        $data = substr($data, strpos($data, '</a>')+4);
        return $result;
    }

    function fetch_menu_contents(&$data, $item="", $start="<@menu-start@>", $end="<@menu-end@>"){
		global $str;
        $str = $data;
		$pos = $item=="" ? 0 : strpos($str, $item);
		if($pos !== false){
			$data = substr($str, 0, $pos+strlen($item));
			$str = substr($str, $pos+strlen($item));
		} else {
			return "";
		}
		if ($str != ""){
			$s_pos = strpos($str, $start);
			if($s_pos !== false){
				$e_pos = get_end_position($s_pos + 1, 1, $start, $end);
				if($e_pos !== false){
					$data = $data . substr($str, 0, $s_pos) . substr($str, $e_pos + strlen($end));
					$str = substr($str, $s_pos + strlen($start), $e_pos - ($s_pos + strlen($start)) );
				} else {
					$data .= $str;
					return "";
				}
			}
		}
		return $str;
	}
	function get_end_position($pos, $depth, $start, $end){
		global $str;
		$s_pos = strpos($str, $start, $pos);
		$e_pos = strpos($str, $end, $pos);
		if($e_pos == ""){
			return false;
		}
		if($s_pos!=""){
			if($s_pos<$e_pos){
				return get_end_position($s_pos + 1, $depth+1, $start, $end);
			}
		}
		if($depth > 1) {
			return get_end_position($e_pos + 1, $depth-1, $start, $end);
		}
		return $e_pos;
    }
    















    

$output0 = '
<section id="mega-menu-solutions" class="container-fluid mega-menu mega-menu-solutions mega-menu-hidden">
    <div class="section-inner">
        <div class="col-lg-12 menu-top-language-responsive"><img class="icon-language-responsive" src="'.$imgpath.'icon-languages.svg"><span>EN</span><img class="icon-search" src="'.$imgpath.'icon-search.svg"></div><div class="visible responsive-menu-1 sub-menu"><a href="#">Solutions</a><a class="responsive-menu-btn">&nbsp;&nbsp;</a></div>
        <div class="row menu-content">
            <div class="col-lg-3 mega-menu-left dark-section"><img class="menu-icon" src="'.$imgpath.'icon-menu-solution.svg">
                <p class="menu-title-1">Solutions</p>
                <p class="menu-description">Optical Transport, Access and Software-Defined.&nbsp;A comprehensive portfolio to empower your network !</p>
            </div>
            <div class="col-lg-9 mega-menu-right">
                <div class="row">
                    <div class="col-lg-4 menu-item-column">
                        <p class="menu-title-2"><a href="#">Ekinops360 Solutions<br></a></p><a href="#">Anyhaul Transport</a><a href="#">Core Switching</a><a href="#">Fiber Deep</a><a href="#">Data Center Interconnection</a><a href="#">Encryption</a><a href="#">Single Fiber</a>
                        <a href="#">System Automation</a>
                    </div>
                    <div class="col-lg-4 menu-item-column">
                        <p class="menu-title-2"><a href="#">OneAccess Solutions</a></p><a href="#">Branch office / enterprise solutions<br></a><a href="#">Voice gateway</a><a href="#">SBC</a><a href="#">SDWAN<br></a><a href="#">Business Ethernet </a><a href="#">Wholesales Ethernet</a>
                        <a href="#">Virtualization</a>
                    </div>
                    <div class="col-lg-4 menu-item-column">
                        <p class="menu-title-2"><a href="#">Compose Solutions<br></a></p><a href="#">Compose solution 1</a><a href="#">Compose solution 2</a><a href="#">Compose solution 3</a></div>
                    <div class="col-lg-12 menu-cta-wrapper">
                        <div class="menu-cta-inner"><a class="cta-orange cta-small" href="#">Solutions Overview</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
';






